# Introduction

This project is a prototype, part of the work done for the UVs IR5A/B (University of Technology of Belfort-Montbéliard).

The goal of this prototype is to make the motion of a 3D avatar (created in Unity) possible through a Python script.
This script detects the user's hand (from the webcam), and send the finger count to the Unity program, which then moves the avatar accordingly.

# Installation

The project is composed of two parts : a **Unity program** and a **Python script**.

## Unity Program

Create an Unity Account, then download and install Unity.
Link to their official website : [https://unity.com/fr](https://unity.com/fr).

## Python Script

You will need to install **Python 3** ([https://www.python.org/download/releases/3.0/](https://www.python.org/download/releases/3.0/)).
Then use **Pip** (included in Python 3) to install the following dependencies :

* OpenCV  : `pip install cv2`
* Numpy   : `pip install numpy`
* Imutils : `pip install imutils`
* Sklearn : `pip install sklearn`

# Program launch

You can execute the **launch.cmd** available in the repository. This command file will start the python script, then the unity program (don't need to download Unity, the program has been built for Windows).

**Warning** : the command file can be used only with a Windows OS.

In case you need to launch the program without the command files :
* First, you need to launch the file **finger_recognition.py**, which is the script detecting the finger count and hosting the socket.
* Then, open the folder **unity** with Unity, then start the program.
* The script and the unity program will synchronise, and you will be able to move the robot with your fingers.

Concerning the finger count :
* No fingers : stay still
* 1 finger   : move forward
* 2 fingers  : rotate right
* 3 fingers  : rotate left
* 4 fingers  : jump