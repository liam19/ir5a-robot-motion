import cv2
import numpy as np
import math
import socket
import logging
import os
import json

# global variables used for socket
host = 'localhost'
port = 50000
backlog = 5
soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
soc.bind((host, port))
soc.listen(backlog)
soc.settimeout(10)


def hand_recognition():
    """
        Detect the number of fingers presented through the user's webcam.
    """
    # start the capture
    cap = cv2.VideoCapture(0)

    # fingers count
    fingers = 0
    fingers_prev = 0

    while cap.isOpened():
        try:  # an error comes if it does not find anything in window as it cannot find contour of max area
            # therefore this try error statement
            _, frame = cap.read()
            frame = cv2.flip(frame, 1)
            kernel = np.ones((3, 3), np.uint8)

            # define region of interest
            roi = frame[50:300, 400:600]

            cv2.rectangle(frame, (400, 50), (600, 300), (0, 255, 0), 0)
            hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

        # define range of skin color in HSV
            lower_skin = np.array([0, 20, 70], dtype=np.uint8)
            upper_skin = np.array([20, 255, 255], dtype=np.uint8)

        # extract skin color imagw
            mask = cv2.inRange(hsv, lower_skin, upper_skin)

        # extrapolate the hand to fill dark spots within
            mask = cv2.dilate(mask, kernel, iterations=4)

        # blur the image
            mask = cv2.GaussianBlur(mask, (5, 5), 100)

        # find contours
            contours, _ = cv2.findContours(
                mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # find contour of max area(hand)
            max_contour = max(contours, key=lambda x: cv2.contourArea(x))

        # approx the contour a little
            epsilon = 0.0005*cv2.arcLength(max_contour, True)
            approx = cv2.approxPolyDP(max_contour, epsilon, True)

        # make convex hull around hand
            hull = cv2.convexHull(max_contour)

        # define area of hull and area of hand
            areahull = cv2.contourArea(hull)
            areacnt = cv2.contourArea(max_contour)

        # find the percentage of area not covered by hand in convex hull
            arearatio = ((areahull-areacnt)/areacnt)*100

        # find the defects in convex hull with respect to hand
            hull = cv2.convexHull(approx, returnPoints=False)
            defects = cv2.convexityDefects(approx, hull)

        # defects_count = no. of defects
            defects_count = 0

        # code for finding no. of defects due to fingers
            for i in range(defects.shape[0]):
                s, e, f, d = defects[i, 0]
                start = tuple(approx[s][0])
                end = tuple(approx[e][0])
                far = tuple(approx[f][0])

                # find length of all sides of triangle
                a = math.sqrt((end[0] - start[0])**2 + (end[1] - start[1])**2)
                b = math.sqrt((far[0] - start[0])**2 + (far[1] - start[1])**2)
                c = math.sqrt((end[0] - far[0])**2 + (end[1] - far[1])**2)
                s = (a+b+c)/2
                ar = math.sqrt(s*(s-a)*(s-b)*(s-c))

                # distance between point and convex hull
                d = (2*ar)/a

                # apply cosine rule here
                angle = math.acos((b**2 + c**2 - a**2)/(2*b*c)) * 57

                # ignore angles > 90 and ignore points very close to convex hull(they generally come due to noise)
                if angle <= 90 and d > 30:
                    defects_count += 1
                    cv2.circle(roi, far, 3, [255, 0, 0], -1)

                # draw lines around hand
                cv2.line(roi, start, end, [0, 255, 0], 2)

            defects_count += 1

            # print corresponding gestures which are in their ranges
            font = cv2.FONT_HERSHEY_SIMPLEX
            if defects_count == 1:
                if arearatio < 7:
                    cv2.putText(frame, '0', (0, 50), font, 2,
                                (0, 0, 255), 3, cv2.LINE_AA)
                    fingers = 0
                else:
                    cv2.putText(frame, '1', (0, 50), font, 2,
                                (0, 0, 255), 3, cv2.LINE_AA)
                    fingers = 1

            elif defects_count == 2:
                cv2.putText(frame, '2', (0, 50), font, 2,
                            (0, 0, 255), 3, cv2.LINE_AA)
                fingers = 2

            elif defects_count == 3:
                if arearatio < 27:
                    cv2.putText(frame, '3', (0, 50), font, 2,
                                (0, 0, 255), 3, cv2.LINE_AA)
                    fingers = 3

            elif defects_count == 4:
                cv2.putText(frame, '4', (0, 50), font, 2,
                            (0, 0, 255), 3, cv2.LINE_AA)
                fingers = 4

            elif defects_count == 5:
                cv2.putText(frame, '5', (0, 50), font, 2,
                            (0, 0, 255), 3, cv2.LINE_AA)
                fingers = 5

            elif defects_count == 6:
                cv2.putText(frame, 'reposition', (0, 50), font,
                            2, (0, 0, 255), 3, cv2.LINE_AA)
                fingers = 0

            else:
                cv2.putText(frame, 'reposition', (10, 50), font,
                            2, (0, 0, 255), 3, cv2.LINE_AA)
                fingers = 0

            # send data through socket if changed during this loop
            if fingers != fingers_prev:
                send_data(fingers)

            fingers_prev = fingers

            # show the windows
            cv2.imshow('frame', frame)
        except:
            pass

        # Press button ESC to stop the script AND the unity program
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break

    # close unity program
    send_data(-1)

    cv2.destroyAllWindows()
    cap.release()


def send_data(fingers: int):
    '''
        Send data through a client socket.
    '''
    try:
        print('Waiting for connection...')
        client, _ = soc.accept()
        print('Connection established')
        data_to_send = json.dumps({'fingers': fingers})
        client.send(bytes(data_to_send, encoding='utf8'))
        print('Data sent')
        client.close()
    except socket.timeout:
        pass
    except:
        raise


if __name__ == "__main__":
    # connect for the 1st time to the Unity client through the socket
    send_data(0)
    # launch the hand recognition
    hand_recognition()
